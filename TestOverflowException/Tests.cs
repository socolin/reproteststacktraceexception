﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace TestOverflowException
{
	public class Tests
	{
		[Test]
		public void Test1()
		{
			Assert.True(true);
		}

		public class TruncateTests
		{
			[TestCase("123", 2, null, "12", TestName = "Name1")]
			[TestCase("123", 2, ".", "1.", TestName = "Name2")]
			[TestCase("12", 2, null, "12", TestName = "Name3")]
			[TestCase("12", 2, ".", "12", TestName = "Name4")]
			[TestCase("12", 5, null, "12", TestName = "Name5")]
			[TestCase(null, 5, null, null, TestName = "Name6")]
			public void CanTruncate(string input, int maxLength, string unused, string expectedResult)
			{
				input.Substring(0, maxLength).Should().Be(expectedResult);
			}
		}
	}
}